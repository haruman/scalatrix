package actor

import akka.actor.{Actor, FSM}
import scalatrix.GUI
import scalatrix.Util.ScalatrixMessage._
import scalatrix.Util.Transform
import scalafx.scene.paint.Color
import scalatrix.Util.ScalatrixState._
import scalatrix.Block
import scalatrix.Block._
import javafx.event.EventHandler
import javafx.scene.input.KeyEvent
import javafx.scene.input.KeyCode

/**
 * The view. Displays falling blocks and game board.
 */
class View extends Actor with FSM[State, (Block, Block)] {
  GUI.stage.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler[KeyEvent] {
    override def handle(evt: KeyEvent): Unit = {
      evt.getCode() match {
        case KeyCode.LEFT =>
          context.parent ! TransformBlock(Transform.MoveLeft)
        case KeyCode.RIGHT =>
          context.parent ! TransformBlock(Transform.MoveRight)
        case KeyCode.UP =>
          context.parent ! TransformBlock(Transform.RotateLeft)
        case KeyCode.DOWN =>
          context.parent ! TransformBlock(Transform.MoveDown)
        case _ => ()
      }
    }
  })

  def fill(block: Block, color: Color): Unit = {
    GUI.rects.filterKeys(block.fields).values.foreach({_.fill = color})
  }


  startWith(Active, ShapeNone -> ShapeNone)

  when(Active) {
    // update the display of the falling block
    case Event(UpdateBlock(newBlock), Tuple2(oldBlock, oldBoard)) => {
      fill(oldBlock, Color.GRAY)
      fill(newBlock, Color.BLUE)
      stay using Tuple2(newBlock, oldBoard)
    }
    // update the display of the gameboard
    case Event(UpdateBoard(newBoard), Tuple2(oldBlock, oldBoard)) => {
      fill(oldBlock, Color.GRAY)
      fill(oldBoard, Color.GRAY)
      fill(newBoard, Color.GREEN)
      stay using Tuple2(ShapeNone, newBoard)
    }
  }
}