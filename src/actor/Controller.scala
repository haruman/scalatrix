package actor

import scala.concurrent.duration.FiniteDuration
import akka.actor.Actor
import akka.actor.Props
import scalatrix.Util.ScalatrixMessage._
import scalatrix.Util.Transform
import scalatrix.GameState


/**
 * The game loop controller.
 * Spawns the [[actor.Model model]] and the [[actor.View view]].
 * Triggers periodic `model` updates and propagates changes between the `model` and `view`.
 */
class Controller extends Actor {

  var interval = FiniteDuration(500, "ms")
  val model = context.actorOf(Props[Model], "model")
  val view = context.actorOf(Props[View].withDispatcher("javafx-dispatcher"), "view")

  override def receive: Actor.Receive = {
    case Tick => {
      context.system.scheduler.scheduleOnce(interval, self, Tick)(context.dispatcher)
      model ! TransformBlock(Transform.MoveDown)
    }
    case msg@TransformBlock(_) => model ! msg

    case msg@UpdateBlock(_) => view ! msg
    case msg@UpdateBoard(GameState.EmptyBoard) => {
      interval = FiniteDuration(500, "ms")
      view ! msg
    }
    case msg@UpdateBoard(_) => {
      interval = FiniteDuration(math.max(interval.toMillis - 5, 100), "ms")
      view ! msg
    }
    case _ => ()
  }
}