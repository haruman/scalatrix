package scalatrix

import actor.Controller
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.Actor
import Util.ScalatrixMessage._
import javafx.embed.swing.JFXPanel

/**
 * Main entry point for the scalatrix application.
 * Initializes the [[akka.actor.ActorSystem ActorSystem]] as well as the
 * main [[actor.Controller game loop controller]].
 */
object Main extends App {

  override def main(args: Array[String]): Unit = {
    new JFXPanel(); // trick: create empty panel to initialize toolkit
    new Thread(new Runnable() {
      override def run(): Unit = {
        GUI.main(Array[String]())
      }
    }).start()

    val system = ActorSystem("Scalatrix")
    val controller = system.actorOf(Props[Controller], "controller")
    controller.tell(Tick, Actor.noSender)
  }
}