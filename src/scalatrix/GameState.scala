package scalatrix

import Util._
import Util.Transform._
import scala.collection.SortedSet

object GameState {
  import scalatrix.TileOrdering._

  val EmptyBoard: Block = Block(0f <> 0f, SortedSet.empty[Tile[Int]])

  def apply(): GameState = GameState(None, EmptyBoard)
}

/**
 * Class containing the actual business(tetris) logic.
 * @param block the currently falling block, if it exists
 * @param board the current game board
 */
case class GameState(block: Option[Block], board: Block) {

  private def inBounds(block: Block): Boolean = {
    (block.min.x >= 0) && (block.max.x <= ROWS - 1) &&
      (block.min.y >= 0) && (block.max.y <= COLS - 1)
  }

  def transform(transform: Transform): GameState = {
    val block = this.block.getOrElse(Block().translate(0 <> COLS / 2 - 2))
    val transformedBlock = block.transform(transform)

    var adjustedBlock = transformedBlock
    // wall kicks
    if (transform == Transform.RotateLeft || transform == Transform.RotateRight) {
      if (!inBounds(adjustedBlock))
        adjustedBlock = transformedBlock.transform(Transform.MoveRight)
      if (!inBounds(adjustedBlock))
        adjustedBlock = transformedBlock.transform(Transform.MoveLeft)
    }
    val intersects = adjustedBlock.intersects(board)
    if (intersects || !inBounds(adjustedBlock))
      adjustedBlock = block

    // down movement; collision checks
    if (transform == Transform.MoveDown) {
      if (intersects && (block.min.x <= 1)) { // collision at top -> Game Over
        return GameState(None, GameState.EmptyBoard)
      } else if (intersects || (block.max.x == ROWS - 1)) { // collision at bottom -> add to board & remove full rows
        return GameState(None, (board <++ block).squashDown())
      }
    }

    // regular case: updated block position
    GameState(Some(adjustedBlock), board)
  }
}