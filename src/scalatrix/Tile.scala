package scalatrix

object TileOrdering {
  class TileOrdering[T : Ordering] extends Ordering[Tile[T]] {
    override def compare(a: Tile[T], b: Tile[T]): Int = {
      val comp = implicitly[Ordering[T]].compare _
      val diffX = comp(a.x, b.x)
      if (diffX != 0) diffX
      else comp(a.y, b.y)
    }
  }
  implicit def tileOrdering[T : Ordering]: Ordering[Tile[T]] = new TileOrdering[T]()
}

/**
 * A (x,y) tuple.
 * @param x the x coordinate
 * @param y the y coordinate
 * @param num the [[scala.math.Numeric Numeric]] typeclass
 * @tparam T  the type of coordinates
 */
case class Tile[T](x: T, y: T)(implicit num: Numeric[T]) {
  import num.mkNumericOps
  import num.mkOrderingOps

  def +(other: Tile[T]): Tile[T] = Tile(this.x + other.x, this.y + other.y)

  def -(other: Tile[T]): Tile[T] = Tile(this.x - other.y, this.y - other.y)

  def *(other: Tile[T]): Tile[T] = Tile(this.x * other.y, this.y * other.y)

  def unary_-(): Tile[T] = Tile(-this.x, -this.y)

  def rotateRight[O <% T : Numeric](o: Tile[O]): Tile[T] = {
    val numO = implicitly[Numeric[O]]
    // rotate right around origin
    // x' = -y + o.x + o.y (-tileSize)
    // y' =  x - o.x + o.y
    Tile(-y + numO.plus(o.x, o.y), x + numO.plus(numO.negate(o.x), o.y))
  }

  def rotateLeft[O <% T : Numeric](o: Tile[O]): Tile[T] = {
    val numO = implicitly[Numeric[O]]
    // rotate left around origin
    // x' =  y + o.x - o.y
    // y' = -x + o.x + o.y (-tileSize)
    Tile(y + numO.plus(o.x, numO.negate(o.y)), -x + numO.plus(o.x, o.y))
  }

  def min(other: Tile[T]): Tile[T] = Tile(x min other.x, y min other.y)

  def max(other: Tile[T]): Tile[T] = Tile(x max other.x, y max other.y)

  override def toString = s"($x, $y)"
}
